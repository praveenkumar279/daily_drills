const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
    },
    {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
    },
    {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
    },
    {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

    },
    {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
    }
];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/ 



// 1. Get all items that are available 


const itemsAvailable = items.map((fruits) => {
    return fruits.name;
});

console.log(itemsAvailable);



// 2. Get all items containing only Vitamin C


const itemsContainingOnlyVitaminC = items.filter((fruits) => {
    return fruits.contains === "Vitamin C";
});

console.log(itemsContainingOnlyVitaminC);




// 3. Get all items containing Vitamin A

const itemsContainingVitaminA = items.filter((fruits) => {
    return fruits.contains.includes("Vitamin A");
});

console.log(itemsContainingVitaminA);



// 4. Group items based on the Vitamins that they contain


const itemsBasedOnVitamin = {};

const groupItems = items.map((fruits) => {

    if(!itemsBasedOnVitamin[fruits.contains]){
        itemsBasedOnVitamin[fruits.contains] = [fruits.name];
    }
    else{
        itemsBasedOnVitamin[fruits.contains] = itemsBasedOnVitamin[fruits.contains].push(fruits.name);
    }
});

console.log(itemsBasedOnVitamin);




// 5. Sort items based on number of Vitamins they contain

const sortBasedOnNoOfVitamins = items.sort((value1, value2) => {
    return value1.contains.length - value2.contains.length;
});

console.log(sortBasedOnNoOfVitamins);
