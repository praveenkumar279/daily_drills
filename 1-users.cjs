const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
};


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/ 





//1. Find all users who are interested in playing video games.

const usersInterestInVideoGames = Object.entries(users).filter(([name, details]) => {
    let userInterestGames = details.interests.toString();
    return userInterestGames.includes("Video Games");
 });

 console.log("\n Interested in Video Games:\n\n" , usersInterestInVideoGames);




 // 2. Find all users staying in Germany.

 const usersStayingInGermany = Object.entries(users).filter(([name, details]) => {
    return details.nationality === "Germany";
 });

 console.log("\n Users staying in germany :\n\n", usersStayingInGermany);





// 3. Sort users based on their seniority level 
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10


const usersBasedOnSeniorityLevel = Object.entries(users).sort(([name1, value1], [name2, value2]) => {
    if(value1.desgination.startsWith("Senior")){
        if(value2.desgination.startsWith("Intern") || !value2.desgination.startsWith("Senior")){
            return -1;
        } else {
            return value2.age - value1.age;
        } 
    }
    else if(value1.desgination.startsWith("Intern")){
        if(!value2.desgination.startsWith("Intern")){
            return 1;
        }
        else{
            return value2.age - value1.age;
        }
    }
    else if(!value1.desgination.startsWith("Senior") && !value1.desgination.startsWith("Intern")){
        if(value2.desgination.startsWith("Senior")){
            return 1;
        }
        else if(value2.desgination.startsWith("Intern")){
            return -1;
        }
        else{
            return value2.age - value1.age;
        }
    }
});

console.log("\n Sorted based on their seniority level : \n\n", usersBasedOnSeniorityLevel);




 // 4. Find all users with masters Degree.


 const usersWithMastersDegree = Object.entries(users).filter(([name, details]) => {
    return details.qualification === "Masters";
 });

 console.log("\n users with Master Degree: \n\n", usersWithMastersDegree);





 // 5. Group users based on their Programming language mentioned in their designation.

 const usersBasedOnProgrammingLanguages = Object.entries(users).reduce((result, [name, details]) => {
    if(details.desgination.startsWith("Senior")){
        details.desgination = details.desgination.split(" ")[1];
    }
    if(details.desgination.startsWith("Intern")){
        details.desgination = details.desgination.split(" ")[2];
    }
    if(!details.desgination.startsWith("Senior") && !details.desgination.startsWith("Intern")){
        details.desgination = details.desgination.split(" ")[0];
    }

    if(result[details.desgination] === undefined){
        result[details.desgination] = [];
    } 

    let allDetails = {};
    allDetails[name] = {};
    allDetails[name] = {...details};
    
    result[details.desgination].push(allDetails);
    
    return result;

 }, {});

 console.log("\n Users based on Programming languages: \n\n",usersBasedOnProgrammingLanguages);
