const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        },{
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/ 


// 1. Find all the items with price more than $65

const itemsWithPriceMoreThan65 = products.map((item) => {
    let allItems =  Object.entries(item).filter(([itemName, itemDetails]) => {
        if(Array.isArray(itemDetails)) {

            let insideAllItemDetails = itemDetails.map((itemNames) => {  

                let insideSingleItem = Object.entries(itemNames).filter((insideItemDetails) => {
                    return Number(insideItemDetails[1].price.replace("$", "")) > 65;
                });
            });
            return insideAllItemDetails;
        }
        else{
            return Number(itemDetails.price.replace("$", "")) > 65;
        }

    });
    return allItems;
});

console.log(...itemsWithPriceMoreThan65);




// 2. Find all the items where quantity ordered is more than 1.


const quantityOrderedMoreThan1 = products.map((item) => {
    let allItems =  Object.entries(item).filter(([itemName, itemDetails]) => {
        if(Array.isArray(itemDetails)) {

            let insideAllItemDetails = itemDetails.map((itemNames) => { 

                let insideSingleItem = Object.entries(itemNames).filter((insideItemDetails) => {
                    return insideItemDetails[1].quantity > 1;
                });
            });
            return insideAllItemDetails;
        }
        else{
            return itemDetails.quantity > 1;
        }

    });
    return allItems;
});

console.log(...quantityOrderedMoreThan1);

