const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/ 


// 1. Find all the movies with total earnings more than $500M.

const moviesWithMoreThan500M = Object.entries(favouritesMovies).filter(([movieName, movieDetails]) => {
    return movieDetails.totalEarnings > "$500M";
});

console.log("\n Movies with total earnings more than $500M : \n\n", Object.fromEntries(moviesWithMoreThan500M));



// 2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.

const movieGotMoreThan3OscarNominations = Object.entries(favouritesMovies).filter(([movieName, movieDetails]) => {
    return movieDetails.oscarNominations > 3;
});

console.log("\n Movies that got more than 3 oscar nominations: \n\n", Object.fromEntries(movieGotMoreThan3OscarNominations));



// 3. Find all movies of the actor "Leonardo Dicaprio".

const allMoviesOfActorLeonardoDicaprio = Object.entries(favouritesMovies).filter(([movieName, movieDetails]) => {
    return (movieDetails.actors).includes("Leonardo Dicaprio");
});

console.log("\n All movies of actor Leonardo Dicaprio: \n\n", Object.fromEntries(allMoviesOfActorLeonardoDicaprio));



// 4. Sort movies (based on IMDB rating)
//    if IMDB ratings are same, compare totalEarning as the secondary metric.

const sortOnImdbRatingAndTotalEarnings = Object.entries(favouritesMovies).sort(([movieName1, movieDetails1], [movieName2, movieDetails2]) => {
    return movieDetails2.imdbRating - movieDetails1.imdbRating || Number(movieDetails1.totalEarnings.replace("$", "").replace("M", "")) - Number(movieDetails2.totalEarnings.replace("$", "").replace("M", ""));
});

console.log("\n sort movies on Imdb rating and total earnings : \n\n", Object.fromEntries(sortOnImdbRatingAndTotalEarnings));


// 5. Group movies based on genre. Priority of genres in case of multiple genres present are:
//    drama > sci-fi > adventure > thriller > crime


const groupMoviesBasedOnGenreProperty = Object.entries(favouritesMovies).reduce((result, [movieName, movieDetails]) => {
    const genrePriority = ["crime", "thriller", "adventure", "sci-fi", "drama"];
    let genreIndex = [];
    
    if(movieDetails.genre.length === 1) {
        if(result[movieDetails.genre[0]] === undefined) {
            result[movieDetails.genre[0]] = [];
        }
        result[movieDetails.genre[0]].push(movieName,{...movieDetails});
    } else {

        movieDetails.genre.map((genres) => {
            genreIndex.push(genrePriority.indexOf(genres));
            return genreIndex;
        });
        let highestPriorityGenre = Math.max(...genreIndex);

        if(result[genrePriority[highestPriorityGenre]] === undefined) {
            result[genrePriority[highestPriorityGenre]] = [];
        }

        result[genrePriority[highestPriorityGenre]].push(movieName,{...movieDetails});

    }
    return result;

}, {});

console.log("\n Group movies based on genre property : \n\n", groupMoviesBasedOnGenreProperty);
